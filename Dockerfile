#Download base image ubuntu 16.04
FROM ubuntu:18.04

RUN apt update &&  apt install -y inetutils-telnet iputils-ping \
  && rm -rf /var/lib/apt/lists/*

#COPY ./docker-entrypoint.sh /docker-entrypoint.sh
#ENTRYPOINT ["./docker-entrypoint.sh"]
#CMD ["/bin/ping","localhost"]
CMD exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
